//
//  Task.h
//  Lab3
//
//  Created by Tobias Ednersson on 2015-02-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// Represents a task to be solved

#import <Foundation/Foundation.h>

@interface Task : NSObject <NSCoding>

@property (nonatomic) NSString * taskdesc;
@property (nonatomic) NSDate  * duedate;
@property (nonatomic) int priority;

- (instancetype)initWith:(NSString*) description duedate:(NSDate *) date priority:(int) priority;

@end
