//
//  TaskController.m
//  Lab3
//
//  Created by Tobias Ednersson on 2015-02-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "TaskController.h"
#import "ToDoBackend.h"//;
#import "Task.h"//;
#import "DetailViewController.h"//;

@interface TaskController()
@property (nonatomic) ToDoBackend * backend;
@property (nonatomic) NSDateFormatter * formatter;



@end

@implementation TaskController

-(NSDateFormatter *) formatter {
    
    if(!_formatter){
       _formatter = [[NSDateFormatter alloc] init];
        [_formatter setDateFormat:@"dd-mm-yy"];
        
        

    }
    
    return _formatter;
}


-(void) viewDidLoad {
    
    if(!self.backend.tasklist || self.backend.tasklist.count == 0) {
        [self.backend addTask:[self.backend createTaskwithdescription:@"important task number 1" priority:3 duedate:@"15/01/01"]];
        [self.backend addTask:[self.backend createTaskwithdescription:@"Another task" priority:1 duedate:@"14/01/01"]];
    }
    
        
        
        /*Task * t = [[Task alloc] initWith:@"important Task" duedate: date priority:1];
        [self.backend addTask:t];*/
        
        
        //[self.backend.tasklist addObject:t];

        
        
    
}


- (ToDoBackend *) backend {
    
    if(!_backend) {
        
        _backend = [ToDoBackend GetInstance];
    }
    
    return _backend;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}



- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.backend.tasklist.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"MyCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    
    Task * t = self.backend.tasklist[indexPath.item];
    
    

    NSString * date = [self.formatter stringFromDate:t.duedate];
  
    
    if(t.priority > 2) {
        cell.backgroundColor = [UIColor redColor];
    }
    
    UITapGestureRecognizer *tapRecognizer =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    
    [cell addGestureRecognizer:tapRecognizer];
    cell.textLabel.text =   [NSString stringWithFormat:@"%@   %@",date,t.taskdesc];
    return cell;
}




- (void) onTap: (UIGestureRecognizer *) tapped {
    
    
    UITableViewCell * cell =  (UITableViewCell *) tapped.view;
    
    [self performSegueWithIdentifier:@"showDetail" sender:cell];
    
    
}




-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"showDetail"]) {
        
        UITableViewCell * cell = (UITableViewCell *) sender;
        
       long selected =  [self.tableView indexPathForCell:cell].item;
        
       DetailViewController * d =  (DetailViewController *)segue.destinationViewController;
        
        [d prepeareView: (int) selected];
        
    }
    
    
}







@end
