//
//  ViewController.m
//  Lab3
//
//  Created by Tobias Ednersson on 2015-02-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "ViewController.h"
#import "ToDoBackend.h"//;
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *DueDate_text;
@property (weak, nonatomic) IBOutlet UISlider *Priority_slider;
@property (weak, nonatomic) IBOutlet UITextField *Description_text;

@property (nonatomic) ToDoBackend * backend;

@end

@implementation ViewController
- (IBAction)AddItem:(id)sender {
    int priority =  (int)  self.Priority_slider.value;
    
    NSString * descriptiontext = self.Description_text.text;
    NSString * duedate = self.DueDate_text.text;
    
    
    
    //create task and add it to the backend
    Task * t = [self.backend createTaskwithdescription:descriptiontext priority:priority duedate:duedate ];
    [self.backend addTask:t];
    
    //Go back to table view
    
    @try {
    [self performSegueWithIdentifier:@"table_segue" sender:self];
    }
    
    @catch(NSException * e) {
    
        NSLog(@"%@",e);
    
    }
    
  
    
    
}



        
    



-(ToDoBackend *) backend {
    
    if(!_backend) {
        _backend = [ToDoBackend GetInstance];
    }
    
    return _backend;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
