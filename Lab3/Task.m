//
//  Task.m
//  Lab3
//
//  Created by Tobias Ednersson on 2015-02-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "Task.h"

@interface Task()

@property (nonatomic, readonly)NSTimeInterval SecondsPerDay;
@end
@implementation Task
@synthesize taskdesc = _taskdesc;
@synthesize priority = _priority;
@synthesize duedate = _duedate;
@synthesize SecondsPerDay = _SecondsPerDay;



- (NSTimeInterval) SecondsPerDay {
    
    if(!_SecondsPerDay != 24*60*60) {
        
        _SecondsPerDay = 24 * 60 * 60;
        
    }
    
    
    
    return _SecondsPerDay;
}

- (NSString *) taskdesc {
    
    if(!_taskdesc) {
        
        _taskdesc = @"Ingen beskrivning för denna uppgift";
    }
    
    return _taskdesc;
}





-(void) setTaskdesc:(NSString *)t {
    
    if(t != nil) {
    _taskdesc = t;
    }
    
}

-(int) priority {

    return _priority;
}

-(void) setPriority:(int)priority {
    
    if(priority > -1) {
        _priority = priority;
    }
    
}

- (NSDate *) duedate {
    
    if(!_duedate){
     
        //by default set all tasks to be finished by tomorrow...
        _duedate = [[NSDate alloc]init];
        _duedate = [_duedate dateByAddingTimeInterval:self.SecondsPerDay];
        
    }
    
    return _duedate;
    
}


- (void)setDuedate:(NSDate *)duedate {
    
    _duedate = duedate;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    NSNumber * prio = [NSNumber numberWithInt:self.priority];
    
    [encoder encodeObject:prio forKey:@"priority"];
    [encoder encodeObject:self.taskdesc forKey:@"taskdesc"];
    [encoder encodeObject:self.duedate forKey:@"duedate"];
}


- (id)initWithCoder:(NSCoder *)decoder
{
    
    self = [super init];
    
    if(self) {
        
        self.taskdesc = [decoder decodeObjectForKey:@"taskdesc"];
        self.duedate = [decoder decodeObjectForKey:@"duedate"];
        NSNumber * num = [decoder decodeObjectForKey:@"priority"];
        self.priority = num.intValue;
        
        //self.priority = [decoder decodeObjectForKey:@"priority"];
        
        
    }
    
    return self;
    
    
}





- (instancetype)initWith:(NSString*) description duedate:(NSDate *) date priority:(int) prio {
    
    
    self = [super init];
    
    
    if(self) {
        
        
        self.priority = prio;
        self.duedate = date;
        self.priority = prio;
        self.taskdesc = description;
        
    
    }
    
    
    return self;
    
}




@end
