//
//  ToDoBackend.m
//  Lab3
//
//  Created by Tobias Ednersson on 2015-02-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// Contains logic for adding removing and saving tasks

#import "ToDoBackend.h"

#import "Constants.h"//;

@interface ToDoBackend ()



@end


@implementation ToDoBackend



/*
 Constructs a new task
 with description,priority and duedate
 
 */


- (instancetype) init {
    
    self = [super init];
    return self;
    
}





// returns the instace of this backend (a singleton)
+(ToDoBackend *) GetInstance {
    
    static ToDoBackend* instance;
    
    if(!instance)
    {
        instance = [[ToDoBackend alloc]init];
    }
    
    return instance;
}








- (int) Sections {
    _Sections = 1;
    
    return _Sections;
}



/*
    Loads saved tasks
 */
- (NSMutableArray *) loadTasks {
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray * array = [defaults arrayForKey:SAVEKEY];

    if(!array) {
        
        return nil;
    }
    
    
    NSMutableArray * objects = [[NSMutableArray alloc] init];
    for(int i = 0; i < array.count; i++) {
        
        @try{
        [objects addObject:(Task*)[NSKeyedUnarchiver unarchiveObjectWithData: array[i]]];
    }
    
        @catch(NSException * e) {
            NSLog(@"%@",e);
        }

    
    }

    
    return objects;
    
    
    
}


/* Saves current tasks on the list
 */
- (void) save {
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    
    NSMutableArray * toSave = [[NSMutableArray alloc]init];
    
    
    
    
    //[self.tasklist copy];
    for(int i = 0; i< self.tasklist.count ; i++) {
    
        @try {
        [toSave addObject:[NSKeyedArchiver archivedDataWithRootObject:self.tasklist[i]]];
        }
        @catch(NSException * e ) {
            NSLog(@"%@",e);
        }
      
        
        }
   
    
    
    
    NSArray * result = [toSave copy];
    
    
    
    @try {
    [defaults setObject:result forKey:SAVEKEY];
    [defaults synchronize];
    }
    @catch(NSException * e) {
        NSLog(@"%@",e);
        
    }
    
}

- (Task *) createTaskwithdescription: (NSString *) description  priority: (int) prio duedate: (NSString *) duedate {
 
    
    
    //create a new dateformater and assume date of form Year-Month-Day with two digits each
    NSDateFormatter * formater = [[NSDateFormatter alloc] init];
    
    [formater setDateFormat:@"dd/mm/yy"];
    
    NSDate * date = [formater dateFromString:duedate];
    
    Task * toReturn = [[Task alloc]initWith:description duedate:date priority:prio];
        
    return toReturn;
}

- (void) deleteTask:(Task *)t {
    
    [self.tasklist removeObject:t];
    //[self save];
}



    
    
- (NSMutableArray *) tasklist {
    
    if(!_tasklist) {
        
        
        _tasklist = [[NSMutableArray alloc] init];

        
        
       /* NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        
        NSArray * arr = [defaults arrayForKey:SAVEKEY];
        if(arr && (arr.count > 0)) {
            
            _tasklist = [self loadTasks];
        //} */
        
        
        }
    return _tasklist;
}



 /*Adds a new task to todo list
 */
-(BOOL) addTask:(Task *)t {
    
    [self.tasklist addObject:t];
    
    //[self save];
    return YES;
}


        
        
    




@end
