//
//  ToDoBackend.h
//  Lab3
//
//  Created by Tobias Ednersson on 2015-02-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"//;

@interface ToDoBackend : NSObject

@property (nonatomic) NSMutableArray * tasklist;
@property (nonatomic) int Sections;

- (BOOL) addTask:(Task *) t;
- (Task *) createTaskwithdescription: (NSString*) desc priority: (int) prio duedate: (NSString *) duedate;
- (void) deleteTask: (Task *) t;
+ (ToDoBackend *) GetInstance;
- (void) save;
- (NSMutableArray *) loadTasks;

@end
