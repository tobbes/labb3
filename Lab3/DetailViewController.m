//
//  DetailViewController.m
//  Lab3
//
//  Created by Tobias Ednersson on 2015-02-07.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//
// This controlls 

#import "DetailViewController.h"//;
#import "ToDoBackend.h"//;
@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UITextView *task_text;

@property (weak, nonatomic) IBOutlet UILabel *endDate_label;
@property (weak, nonatomic) IBOutlet UILabel *priorityLabel;
@property (nonatomic) ToDoBackend * backend;
@property (nonatomic) int taskid;

@end







@implementation DetailViewController

-(ToDoBackend *) backend {
    
    if(!_backend) {
        _backend = [ToDoBackend GetInstance];
        
    }
    
    return _backend;
}





-(void) prepeareView: (int) taskid {

        self.taskid = taskid;
    
        NSLog(@"tjipp: %d", self.taskid);


}
- (IBAction)Delete:(id)sender {
    
    [self.backend deleteTask:self.backend.tasklist[self.taskid]];
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    }






-(void) viewDidLoad {
    
    Task * task = self.backend.tasklist[self.taskid];
    self.task_text.text = task.taskdesc;
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init ];
    
    [formatter setDateFormat:@"yy/mm/dd"];
    
    self.endDate_label.text =
    [formatter stringFromDate:task.duedate];
    NSNumberFormatter * numformat = [[NSNumberFormatter alloc] init];
    NSNumber * numberToWrite = [[NSNumber alloc] initWithInt:task.priority];
    
    if(task.priority > 2) {
        self.priorityLabel.textColor = [UIColor redColor];
    }
    
    self.priorityLabel.text =
    [numformat stringFromNumber:numberToWrite];
    
    
    
    
    
    
    
    
    
}


@end
